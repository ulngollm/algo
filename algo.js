function sum(nums, target) {
  const count = nums.length;
  for (let i = 0; i < count; i++) {
    for (let j = i + 1; j < count; j++) {
      let sum = nums[i] + nums[j];
      if (sum == target) return [i, j];
    }
  }
}

function isPalindrome(x) {
  if (x < 0) return false;
  const digits = `${x}`.split("");
  let reverseNum = "";
  for (let i = digits.length - 1; i >= 0; i--) {
    reverseNum += digits[i];
  }
  return reverseNum == x;
}

const dict = {
  I: 1,
  V: 5,
  X: 10,
  L: 50,
  C: 100,
  D: 500,
  M: 1000,
};

function romanToInt(s) {
  const entries = s.split("");
  let num = 0;
  for (let i = entries.length - 1; i >= 0; i--) {
    const nextIndex = i + 1;
    value = dict[entries[nextIndex]] > dict[entries[i]] ? -dict[entries[i]] : dict[entries[i]];
    num += value;
  }
  return num;
}


