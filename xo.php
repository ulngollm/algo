<?php

function XO($str)
{
  $re = '/(x)|(o)/mi';
  preg_match_all($re, $str, $matches);

  $res1 = array_filter($matches[1], function ($item) {
    return $item;
  });
  $res2 = array_filter($matches[2], function ($item) {
    return $item;
  });
  print_r($matches);
  return count($res1) == count($res2);
}

function toCamelCase($str){
  $res = preg_split('/[-_]/', $str);
  array_walk($res, function(&$item, $index){
    if($index > 0)  $item = ucfirst($item);
  });
  return implode('', $res);
}

echo toCamelCase('aasd-ads-we');