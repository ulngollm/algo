const bracketsPair = {
    '{': '}',
    '(':')',
    '[':']'
};

class Stack {
    constructor(){
        this.size = 0;
        this.data = [];
    }
    push(value) {
        this.size++;
        this.data[this.size] = value;
    }
    pop(){
        const value = this.data[this.size];
        this.size--;
        return value;
    }
}

function parseString(string) {
    let stack = new Stack();
    const symbols = string.split('');
    for(let symbol of symbols){
        if (bracketsPair.hasOwnProperty(symbol)){
            stack.push(bracketsPair[symbol]);
        } else {
            const closestClosedBracket = stack.pop();
            if(symbol != closestClosedBracket) {
                return false;
            };
        }
    }
    return stack.size == 0? true: false;
}


console.log(parseString('}'));